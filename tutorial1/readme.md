# Tutorial 1
# I Don't Want My Magic to Fail so I'll Test It First with Unit Testing
It is 8 o'clock in the morning. The air is fresh and the sun is shining. Guild master sits in front of you, drinking a cup of coffee. Today, Guild Master wants to show you another legacy system that the guild has: Employee Registration System. The guild usually uses it to add new staff when they join the guild. He wants you to check on the system that had been developed by former guild wizards.

When you looked at the system, you feel like it lacks something. Suddenly, your vision becomes dark, you feel a sharp pain in your head, and feels like you are slowly sinking into the ground. It occurs for one minute before it stops. You feel some knowledge forced in to your head. 

" Unit testing..."

Your mouth utters the words painfully.

" This system lacks unit testing.." you continue.

You look at Guild Master's confused face. As if he were listening to some other language he didn't understand. Then, he asks you what does unit testing mean. 

" I will show you."

## Unit Testing
You take a look at the code and see a bunch of Java files. They look like this:
```
controller
	EmployeeController
model
	GuildClerk
	GuildEmployee
	GuildMasterStaff
repository
	GuildEmployeeMemoryRepository
	GuildEmployeeRepository
service
	GuildEmployeeService
	GuildEmployeeServiceImpl
Tutorial1Aplication
```
You think that showing a unit test for a controller will be difficult for a first timer. So you decide to test the service first to make Guild Master understand the significance of unit testing.

"Unit testing exists for quality control of the application.." you start explaining unit testing to Guild Master. Guild Master still looks unconvinced, but listens anyway.

The main goal of unit testing is to **divide** parts of the program, and **test them individually**. You can isolate and check a portion of the code and see if it behaves as intended. It is easy to find out if something is wrong in a small part of the code, rather than testing an integrated module.

You elaborate that unit testing is important because it is one of the **earliest testing efforts** you can perform, and the quicker you can detect problems in the code, the faster you can amend them. Detecting bugs early in the code is also the most **cost-efficient** method when developing a project, as the later you fix code, usually the more expensive it gets to do so. Guild Master's eyes lit up when you mentioned cost-efficiency.

You show him an example of a unit test.
```java
package id.ac.ui.cs.tutorial1.service;  
  
import id.ac.ui.cs.tutorial1.model.GuildClerk;  
import id.ac.ui.cs.tutorial1.model.GuildEmployee;  
import org.junit.jupiter.api.BeforeEach;  
import org.junit.jupiter.api.Test;  
  
import static org.junit.jupiter.api.Assertions.*;  
  
class GuildEmployeeServiceImplTest {  
  
  
  private GuildEmployeeServiceImpl guildEmployeeService;  
  
 @BeforeEach void setUp() {  
  guildEmployeeService = new GuildEmployeeServiceImpl();  
  }  
  
  @Test  
 void testAddValidInputEmployeeWithTypeClerkShouldReturnClerkInstance() {  
  GuildEmployee shouldBeClerkInstance = guildEmployeeService.addEmployee("Hans", "MacBeth", "1998-02-21", "Clerk");  
  assertTrue(shouldBeClerkInstance instanceof GuildClerk);  
  }
```
" A unit test is also in the form of a class, in this case we want to test GuildEmployeeServiceImpl, then the test class will be named `GuildEmployeeServiceImpl`. "  You start explaining as you create this world's very first unit test. 

"A test case is represented by a method in the Unit Test Class, and the name of the methods are the name of test case. In this test case, I want to make sure that when I give this method a valid input with Type 'Clerk', it should return a Clerk instance by magic summoning. "

You see a change in the Guild Master's face. Now, he appears to be intrigued with this new type of magic he had never known before. 
" So we can replace manual checking by using this magic.. That's you want to tell me, don't you?" 
" Yes.." you briefly answer the question. 

As you see a sudden optimistic face in front of you, you unconsciously smile. The face of Guild Master becomes more and more curious as his interest grows in this foreign magic.
" How do we choose a test case? I am sure there are infinite possibilities when creating a test case.. " he asked.

" I am glad you asked.."
You start to explain the concept: `Choosing Test Cases by Partitioning`.

When you look at the `addEmployee` method, it has four parameters. `String name`, `String familyName`, `String birthDate`, and `String type`.  As you understand it, there are some possibilities in the four arguments. 

All of them are `String`, so the possibilities are as follows:
1. The input is an empty String (`""`)
2. The input is a non-empty String.
3. The input is a String Maximum Input (. 2<sup>31</sup>-1)
4. The input is a null object.
Also notice, the rules for the name in the input shouldn't be an empty String. With that, we can make test cases that cover all the possibilities above. 

Take a look at our current test, it has covered number 2: The input is a non-empty String. Then we need to cover more test cases!
```
Quest 1: Create more test cases for addEmployee that covers all the possibilities above!
```  
After done creating tests, you'll realize that the tests have failed! Guild Master looks confused as he sees the failed unit testing. 

You then try the system directly, running the ` Tutorial1Application.java`, opening localhost:8080/employee/add

When you try inserting empty String or null (no entry), it succeeds. The latter even raises an exception. This shouldn't be happening. 
" May I fix the error, Guild master? "
He nodded.  

Open `tutorial1/src/main/id.ac.ui,cs.tutorial1/service/GuildEmployeeServiceImpl`
Observe `addEmployee`method.
Add handling to empty and null object input.

`Quest 2: Change the implementation so it doesn't accept empty string and null object. (You don't need to create front-end implementation, just make sure it doesn't save the data with above input to the memory ) `

The last thing you notice, `birthDate` is a special case.
It needs to match the pattern stated in the method. You will notice it has several case addition besides the case we have defined above.
1. It matches the pattern `yyyy-MM-dd`
2. It doesn't match the pattern.

If analyzed correctly, the case for empty String has covered the case empty String and pattern not matched and then the non-empty string has covered either String not empty and matched(or not matched) the pattern. You just need to complete the one that has not been handled yet. 

`Quest 3:  Complete the remaining test cases possibilities`

If we look at the all possible cases, it will look like this.
` addEmployee(name, familyName, birthDate, type) `
Name has 4 possibilities, and so does `familyName` and `type`. `birthDate` has 4 possibilities plus two special cases. Then it will be : 4*4*4*6 = 384 cases. 

But using the concept of partition, you'll realize you don't need all of that possibilities to be written to test cases. 

There are some test cases that include other cases. By that concept you just need to make sure all the possibilities has been covered and there is no need to make all possible combinations into test cases. You can read [this](https://ocw.mit.edu/ans7870/6/6.005/s16/classes/03-testing/) article to have a deeper understanding to this concept. 


## Automatic Testing

Now that you've learned how to utilize the power of Git to push your commits to [GitLab](https://gitlab.com/) , now you are going to learn how a magic that allows your project to run tests automatically in the online repository works.

You look at the **gitlab-ci.yml** file in the root folder. You see the following:
```yml
#Gitlab CI configuration for Advanced Programming lab exercises

image: gradle:alpine

stages:
 - test
 
# Tutorial 0 CI jobs
test:t0:
 stage: test
 script: gradle :tutorial0:check
```
Looks pretty simple. This file is called a gitlab CI (continuous integration) file. Whenever you commit and push to a repository with a **gitlab-ci.yml** file, GitLab will run a CI pipeline.

The **gitlab-ci.yml** file is basically a list of instructions, and it will tell what the GitLab runner should do. 

The `image` tag  is the type of Docker image that will be used for the project. You don't quite remember what "Docker" is supposed to mean, but you do recall it has something to do with application deployment and DevOps. Because we are using Gradle, the `image` has been set to `gradle:alpine`.

The `stages` tag is used to list the stages we want our pipeline to do. 
Normally, there are three stages in a pipeline:
`build`, `test`, and `deploy`. Not all of these stages need to be used; stages without jobs are ignored. For the current project, we will only be using `test`.

There are a lot more commands if you want your runner to do more complicated things, but you feel like you will study that yourself later. The complete configuration guide can be found [here](https://docs.gitlab.com/ee/ci/yaml/) if you would like to read more about it.

After the stages are defined, you can define the jobs that you want to run during the pipeline. What we want to do is to automatically run the tests in the tutorial folder, so we make a job called `test:t0`, and run the script `gradle :tutorial0:check` with it. 

---
*Note*: You can run the `check` command in your local command line before pushing it to GitLab to check if your tests are correct. Just type in the command `gradle :tutorial0:check` or simply `gradle check` in your project's root folder, and your local Gradle daemon will run tests. Do also note there are no tests in tutorial0, but the runner doesn't detect any errors.

---
You can check if your pipeline has succeeded by looking at the rightmost part of the bar with your commit message on it. If it shows a green check like so, your pipeline has passed. Or a red cross if the pipeline has failed.

![green arrow](images/green.PNG)

You can check your pipelines by going into the CI/CD menu in GitLab. 

![pipelines](images/pipelines.PNG)

You can also view the commands that have run in the pipeline by clicking on the pipeline status. This is where you view errors that happen in the pipeline.

![pipelinecode](images/pipelinecode.PNG)

That's about it! Now whenever you make a commit and push, you can see if you failed the tests or not automatically. You might want to remember the information you just learned for later.

`Quest 4: Make modification to **gitlab-ci.yml** and make the file include tests for this tutorial``

Checklist
- [ ] Completing all the Quest related to test cases.
- [ ] Make modification to **gitlab-ci.yml** to include `tutorial1 `tests.
